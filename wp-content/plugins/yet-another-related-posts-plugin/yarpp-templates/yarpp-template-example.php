<?php
/*
YARPP Template: Starter Template
Description: A simple starter example template that you can edit.
Author: YARPP Team
*/
?>

<?php
/*
Templating in YARPP enables developers to uber-customize their YARPP display using PHP and template tags.

The tags we use in YARPP templates are the same as the template tags used in any WordPress template. In fact, any WordPress template tag will work in the YARPP Loop. You can use these template tags to display the excerpt, the post date, the comment count, or even some custom metadata. In addition, template tags from other plugins will also work.

If you've ever had to tweak or build a WordPress theme before, you’ll immediately feel at home.

// Special template tags which only work within a YARPP Loop:

1. the_score()		// this will print the YARPP match score of that particular related post
2. get_the_score()		// or return the YARPP match score of that particular related post

Notes:
1. If you would like Pinterest not to save an image, add `data-pin-nopin="true"` to the img tag.

*/
?>

<h3>Related Posts</h3>
<?php if ( have_posts() ) : ?>
<ul>
	<?php
	while ( have_posts() ) :
		the_post();
		
	$room_quantity     = get_post_meta(get_the_ID(), 'kbrd_rooms_quantity', true);
		$property_square   = get_post_meta(get_the_ID(), 'kbrd_property_square', true);
		$floor             = get_post_meta(get_the_ID(), 'kbrd_floor', true);
		$floors_quantity   = get_post_meta(get_the_ID(), 'kbrd_floors_quantity', true);
		$image             = get_post_meta(get_the_ID(), 'kbrd_images', true);

		$custom_title = $room_quantity.'-к квартираs, '.$property_square.'м², '.$floor.'/'.$floors_quantity.' эт.';
		echo $custom_title;
		?>
	<?php endwhile; ?>
</ul>
<?php else : ?>
<p>No related posts.</p>
<?php endif; ?>
