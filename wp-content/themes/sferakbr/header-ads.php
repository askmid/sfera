<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sferakbr
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<?php $address           = get_post_meta(get_the_ID(), 'kbd_adress', true);  ?>
	<?php $description       = get_post_meta(get_the_ID(), 'kbrd_description', true); ?>
	<script src="https://api-maps.yandex.ru/2.0-stable/?apikey=7ec005d7-d713-478b-9186-6446eadbd75f&load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
      ymaps.ready(init);
        var myMap,
            myPlacemark;
        var myGeocoder = ymaps.geocode("улица Кирова, 20, Нальчик");    
        function init(){
            myMap = new ymaps.Map ("map", {
                center: [43.482289, 43.583904],
                zoom: 14
            });
            
                // В метод add можно передать строковый идентификатор
    // элемента управления и его параметры.
    myMap.controls
        // Кнопка изменения масштаба.
        .add('zoomControl', { left: 5, top: 5 })
        // Список типов карты
        .add('typeSelector')
        // Стандартный набор кнопок
        .add('mapTools', { left: 35, top: 5 });

             var addrstr = "<?php echo $address ?>";
             var desc = "<?php echo $description ?>";
             
          //  myPlacemark = new ymaps.Placemark([43.482289, 43.583904], { content: 'Новостройка', balloonContent: addrstr });
            myPlacemark = new ymaps.Placemark([43.482289, 43.583904], { content: 'Новостройка', balloonContent: addrstr + "  "+ desc });
            
            myMap.geoObjects.add(myPlacemark);
        }
    </script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'sferakbr' ); ?></a>

	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="main-navigation kbrd-column col1_2">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'sferakbr' ); ?></button>
			<?php
     wp_nav_menu(
      array(
       'theme_location' => 'menu-1',
       'menu_id'        => 'primary-menu',
      )
     );
			?>-
		</nav><!-- #site-navigation -->
  <?php get_template_part( 'template-parts/admin', 'bar' ); ?>
  </div>
  
  
  <div id="myModal">
  <?php echo do_shortcode('[wpuf-login]'); ?>
   <span id="myModal__close" class="close">X</span>
  </div>
  <div id="myOverlay"></div>



	</header><!-- #masthead -->
 <div class="container">
 <div class="site-branding col_flex_center">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title col1_4"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title col1_4"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$sferakbr_description = get_bloginfo( 'description', 'display' );
			if ( $sferakbr_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $sferakbr_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
   <div class="col3_4">
    <?php //echo do_shortcode('[searchandfilter id="211"]'); ?>
   </div>
    <?php echo '<div class="clear"></div>';?>
		</div><!-- .site-branding -->
