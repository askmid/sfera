<?php

/* Lightslider & Lightgallery */
function Slider($x){
 $images = get_post_meta(get_the_ID(), 'kbrd_images', true);
 $size = 'full'; // (thumbnail, medium, large, full or custom size)
  if( $images && is_array($images)): ?>
  <ul class="<?php echo $x; ?>">
   <?php foreach( $images as $image_id ): 
    showImages($image_id);
    endforeach; ?>
  </ul>
 <?php elseif ($images && !is_array($images)): ?>
 <ul class="<?php echo $x; ?>">
  <?php showImages($images); ?>
 </ul>
  <?php else : ?>
   <img src="http://sferakbr.ru/wp-content/uploads/2022/02/default_img3.jpg" />
 <?php endif; ?>
<?php } 

/* Вывод списка изображений в Lightslider */
function showImages($n) { ?>
 <li data-thumb="<?php echo wp_get_attachment_image_url( $n ); ?>" data-src="<?php echo wp_get_attachment_image_url( $n, $size ); ?>">
  <img src="<?php echo wp_get_attachment_image_url( $n, $size ); ?>" />
 </li>
<?php }

/* Получает slugs категорий */
function getCategoriesSlugs()
{
 $categories_list = get_categories();
  if(empty($categories_list)) return[];
  $slugs = [];
  foreach( $categories_list as $cat ){
   $slugs[] = $cat->slug;
  }
  return $slugs;
}

/* Поле КОМИССИЯ не отображается, если поле пустое (сдам кв на длительный срок) */
function notEmptyComission($value, $text)
{
 if ($value) {
  echo "$value <span>$text</span> <br />";
 }
}

/* Разделить цену по 1000 */
function dividePrice($x) {
 echo number_format($x, 0, '.', ' ').' ₽'; 
}
// $price = get_post_meta(get_the_ID(), 'kbrd_price', true);
// number_format($price, 0, '.', ' ');

/* Стоимость м²  */
function calcSquarePrice ($price, $square, $scale) {
 $sum = $price / $square;
 $numberFormat = number_format($sum, 0, '', ' ');
 echo '<span style="font-size:14px;">'.$numberFormat.' ₽ за '.$scale.'</span>';
}

function calcFullPrice ($price, $square, $scale) {
 $sum = $price * $square;
 $numberFormat = number_format($sum, 0, '', ' ');
 echo '<br/><span style="font-size:14px;">'.$numberFormat.' ₽ за '.$scale.'</span>';
}

/* Комиссия  */
function calcComission ($price, $comission) {
 if($comission) {
 $sum = $price / 100 * $comission;
 $numberFormat = number_format($sum, 0, '', ' ');
 echo '<span>, комиссия '.$numberFormat.' ₽</span>';
 }
}

/* Залог */
function getDeposite($deposite) {
if($deposite == 'Без залога') {
 echo $deposite;
} else {
 echo 'Залог: '.$deposite;
}
}

function getDepositeSum($deposite, $price) {
 $title = 'залог: ';
 $r = ' ₽';
 switch ($deposite) {
 case ('Без залога') :
  echo $deposite;
  break;
 case ('0,5 месяца') :
  echo $title, $price / 2, $r;
  break;
 case ('1 месяц') :
  echo $title, $price, $r;
  break;
 case ('1,5 месяца') :
  echo $title, $price * 1.5, $r;
  break;
 case ('2 месяца') :
  echo $title, $price * 2, $r;
  break;
 case ('2,5 месяца') :
  echo $title, $price * 25, $r;
  break;
 case ('3 месяца') :
  echo $title, $price * 3, $r;
  break;
 }
}



/* Заголовок поля */
function itemView($title, $value, $text, $brm) {
  if ($value != '-1' && !empty($value)) echo ('<span>'.$title.' </span>'. $value . $text . $brm);
}

/* Если значение -1 */
function haveValue($x) {
 if ($x != -1) {
  echo($x);
 }
}
/* Параметры цены */
// function getPriceParams($value) {
//  if($value = 'за м²') {
//   calcSquarePrice($price, $property_square, 'м²');
//  } else {
//   calcFullPrice($price, $property_square, 'за все');
//  }
// }

/* Разместить объявление Категории */
function getCategoryList($re){
 $categories = wp_list_categories('exclude=1,379&style=0&child_of='.$re.'&depth=1&hide_empty=0&title_li=');//
  echo $categories;
}

/* Получить имя пользователя */
function getUserName() {
 $user_info = get_userdata(1);
 echo $user_info->user_login; 
}

/* Получить сылку на категорию */
function caLink($n) {
 echo get_category_link($n);
}

/* Редактировать объявление, если автор */
function editAd(){
 $current_user = wp_get_current_user();
 $current_author = get_the_author();

  if ($current_author === $current_user->user_login) {
    echo '<a class="wpuf-posts-options wpuf-posts-edit" href="http://sferakbr.ru/edit/?pid='.get_the_ID().'&'.( wp_nonce_url( $url, 'wpuf_edit' ) ).'"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.2175 0.232507L14.0736 2.08857C14.3836 2.39858 14.3836 2.90335 14.0736 3.21336L12.6189 4.66802L9.63808 1.68716L11.0927 0.232507C11.4027 -0.0775022 11.9075 -0.0775022 12.2175 0.232507ZM0 14.3061V11.3253L8.7955 2.52974L11.7764 5.5106L2.98086 14.3061H0Z" fill="#B7C4E7"/></svg></a>';
  }
}


/* SET THIMBNAIL to post */
function setPostThumb(){
  $post_id = get_the_ID();
  $images = get_post_meta($post_id, 'kbrd_images', true);

  if (empty($images)) return set_post_thumbnail( $post_id, 1614 );

  if(!is_array($images)) return set_post_thumbnail( $post_id, $images );

  return set_post_thumbnail( $post_id, current($images) );

}
/* SET TITLE to post */
function setPostTitle() {
  $post_id = get_the_ID();
  $categories = get_the_category();
  $current_cat_id = $categories[0]->cat_ID;
 
  $custom_title = makeTitle($post_id, $current_cat_id);
  //var_dump($custom_title);
  $my_post = array();
  $my_post['ID'] = $post_id;
  $my_post['post_title'] = $custom_title;
  
  // Обновляем данные в БД
  wp_update_post(wp_slash( $my_post) );
  //var_dump(the_title());
}

function makeTitle($post_id, $cat_id) {
  $meta = get_post_meta($post_id);
  //var_dump($meta);
  if (in_array($cat_id, array(12,13))) return $meta['kbrd_rooms_quantity'][0].'-к квартира, '.$meta['kbrd_property_square'][0].'м², '.$meta['kbrd_floor'][0].'/'.$meta['kbrd_floors_quantity'][0].' эт.';
  return the_title('','',false);
}

function printPostTitle() {
  $post = get_post(get_the_ID());
  echo $post->post_title;
}