<?php include __DIR__ . '/template-parts/meta-fields.php'; ?>
<?php 

?>
<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
   <?php titleView($all_meta) ?>
  </div>
 </div>
 <div class="kbrd-column col2_5">
 <span class="pre-price"><?php calcSquarePrice1($all_meta); ?></span>
  <div class="ad-bold">
   <?php dividePrice($price); ?>
  </div>
 </div>
 <div class="clear"></div>
</div>


<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
  <?php echo $contact_person; ?><br />
  <?php echo $ownership_type; ?><br />
  №: <?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О квартире</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Количество комнат:', $room_quantity, '', $br);?>
   <?php itemView('Общая площадь:', $property_square, ' м²', $br);?>
   <?php itemView('Площадь кухни:', $kitchen_square,' м²', $br);?>
   <?php itemView('Жилая площадь:', $living_square,' м²', $br);?>
   <?php itemView('Этаж:', $floor,'','');?> из <?php echo $floors_quantity; ?><br />
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Тип комнат:', $rooms_type, '', $br);?>
   <?php itemView('Высота потолков:', $ceiling_height, '  м', $br);?>
   <?php itemView('Санузел:', $wc, '', $br);?>
   <?php itemView('Окна:', $window_view, '', $br);?>
   <?php itemView('Отделка:', $finishing, '', $br);?>
   <?php itemView('Балкон/лоджия:', $balcony, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">О доме</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Тип дома: ', $property_type, '', $br);?>
   <?php itemView('Этажей в доме: ', $floors_quantity, '', $br);?>
   <?php itemView('Пассажирский лифт: ', $passanger_elevator, '', $br);?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Грузовой лифт: ', $cargo_elevator, '', $br);?>
   <?php itemView('Двор: ', $outside, '', $br); ?>
   <?php itemView('Парковка: ', $parking, '', $br); ?>
  </div>
  <div class="clear"></div>
 </div>
</div>