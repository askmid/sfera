<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sferakbr
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php //dynamic_sidebar( 'sidebar-1' ); ?>
 <ul>
  <li class="cat-item">
   <a href="<?php caLink(5);?>">Все квартиры</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(12);?>">Квартиры в новостройках</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(11);?>">Квартиры в аренду</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(14);?>">Квартиры постуточно</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(8);?>">Дома, дачи, коттеджи</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(6);?>">Комнаты</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(9);?>">Коммерческая недвижимость</a>
  </li>
  <li class="cat-item">
   <a href="<?php caLink(8);?>">Земельные участки</a>
  </li>
 </ul>
 <?php 

echo do_shortcode( '[searchandfilter id="1483"]' );

 ?>
</aside><!-- #secondary -->
