<?php include ('meta-fields.php'); ?>

<!-- Параметры квартиры -->
<div class="service-title">
 <?php echo do_shortcode('[wbcr_php_snippet id="6012"]'); ?>
</div>
<a href="<?php echo get_permalink(); ?>">
 <div class="service-title">
 <?php echo get_the_title(); ?> <?php echo $property_square; ?> м²<br />
 </div>
</a>
<div class="price">
 <?php echo $price; ?><br/>
 <?php calcSquarePrice($price, $property_square, 'м²'); ?>
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->