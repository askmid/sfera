<?php include __DIR__ . '/../meta-fields.php'; ?>

<!-- Параметры квартиры -->
<div class="ad-slides">
  <?php echo Slider('lSlider'); ?>
</div>

 <div class="service-title">
 <a class="ad-link" href="<?php echo get_permalink(); ?>">
  <?php echo $room_quantity; ?>-к квартира,
  <?php echo $property_square; ?> м²,<br />
  <?php echo $floor; ?>/<?php echo $floors_quantity; ?>
  эт.
  </a>
 </div>

<div class="price">
<?php dividePrice($price); ?>
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->