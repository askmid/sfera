<?php include __DIR__ . '/../meta-fields.php'; ?>

<!-- Параметры квартиры -->
<div class="service-title">
<?php echo Slider('lSlider'); ?>
</div>
<div class="service-title">
 <a class="ad-link" href="<?php echo get_permalink(); ?>">
  <?php echo $housing_type; ?> <?php echo $property_square; ?> м²,<br />
  на участке <?php echo $area_square; ?> сот.
 </a>
</div>
<div class="price">
 <?php dividePrice($price); ?> за сутки
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->