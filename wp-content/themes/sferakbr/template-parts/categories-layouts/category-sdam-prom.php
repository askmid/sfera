<?php include __DIR__ . '/../meta-fields.php'; ?>

<!-- Параметры квартиры -->
<div class="service-title">
<?php echo Slider('lSlider'); ?>
</div>
<div class="service-title">
 <a class="ad-link" href="<?php echo get_permalink(); ?>">
  Участок <?php echo $area_square; ?> сот. (промназначения)
 </a>
 </div>
<div class="price">
 <?php dividePrice($price); ?> в месяц
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->