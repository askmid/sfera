<?php include __DIR__ . '/../meta-fields.php'; ?>

<!-- Параметры квартиры -->
<div class="service-title">
 <?php echo Slider('lSlider'); ?>
</div>
<div class="service-title">
 <a class="ad-link" href="<?php echo get_permalink(); ?>">
   Торговое помещение <?php echo $property_square; ?> м²,
 </a>
 </div>
 <div class="price">
 <?php dividePrice($price); ?>
 <span class="price-params"> <?php calcSquarePrice($price, $property_square, 'м²'); ?></span>
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->

