<?php include __DIR__ . '/../meta-fields.php'; ?>

<!-- Параметры квартиры -->
<div class="service-title">
<?php echo Slider('lSlider'); ?>
</div>
<div class="service-title">
 <a class="ad-link" href="<?php echo get_permalink(); ?>">
  Комната <?php echo $property_square; ?> м²,
  в <?php echo $room_quantity; ?>-к.,<br/>
  <?php echo $floor; ?>/<?php echo $floors_quantity; ?> эт.
 </a>
 </div>
<div class="price">
<?php dividePrice($price); ?> за сутки
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->