<?php include ('meta-fields.php'); ?>

<!-- Параметры квартиры -->
<div class="service-title">
 <?php echo do_shortcode('[wbcr_php_snippet id="6012"]'); ?>
</div>
<a href="<?php echo get_permalink(); ?>">
 <div class="service-title">
  Дом <?php echo $property_square; ?> м²,<br />
 </div>
</a>
<div class="price">
 <?php echo $price; ?> ₽<br/>
</div>
<div class="location">
 <?php echo $address; ?>
</div>
<div class="post-date">
 <?php echo the_date(); ?>
</div>
<!-- END Параметры квартиры -->