<?php /* Template Name: daughter */ 
get_header();
get_sidebar();
$myposts = get_posts( [
	'post_parent' => 10,
] );
foreach( $myposts as $post ){
	setup_postdata( $post );
	?>
	<div class="article-elem">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</div>
	<?php
}
wp_reset_postdata();
get_footer();
?>