<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sferakbr
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class('col1_3 kbrd-column'); ?>>
	<div class="entry-content">
<?php 
    $categories = getCategoriesSlugs();
		foreach ($categories as $cat) {
        	if(in_category($cat)) {
        		include 'categories-layouts/category-'.$cat.'.php';
        		break;
			}
		}
?>
	</div><!-- .entry-content -->
</section><!-- #post-<?php the_ID(); ?> -->
