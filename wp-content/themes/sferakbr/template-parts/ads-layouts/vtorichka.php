<?php include __DIR__ . '/../meta-fields.php'; ?>

<?php setPostThumb(); ?>
<?php setPostTitle(); ?>
<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
  <?php printPostTitle(); ?>
  </div>
  <div class="subtitle">
    <?php echo '<span>'.$address.'</span>'; ?>
  </div>
 </div>
 <div class="kbrd-column col2_5">
  <div class="ad-bold">
   <?php dividePrice($price); ?>
  </div>
  <span class="subtitle"><?php calcSquarePrice ($price, $property_square, 'м²'); ?></span> 
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;"><?php echo $phone; ?></div><br />
 </div>
  <?php echo $contact_person; ?><br />
  <?php echo $ownership_type; ?><br />
  №: <?php echo $ad_id; ?>
 <div class="edit-link">
 <!-- <a class="wpuf-posts-options wpuf-posts-edit" href="http://sferakbr.ru/edit/?pid=<?php echo get_the_ID().'&'.( wp_nonce_url( $url, 'wpuf_edit' ) ); ?>"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.2175 0.232507L14.0736 2.08857C14.3836 2.39858 14.3836 2.90335 14.0736 3.21336L12.6189 4.66802L9.63808 1.68716L11.0927 0.232507C11.4027 -0.0775022 11.9075 -0.0775022 12.2175 0.232507ZM0 14.3061V11.3253L8.7955 2.52974L11.7764 5.5106L2.98086 14.3061H0Z" fill="#B7C4E7"/></svg></a> -->
 </div>
 <?php editAd(); ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О квартире</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Номер квартиры:', $flat_num, '', $br);?>
   <?php itemView('Тип жилья:', $housing_type, '', $br);?>
   <?php itemView('Количество комнат:', $room_quantity, '', $br);?>
   <?php itemView('Общая площадь:', $property_square, ' м²', $br);?>
   <?php itemView('Площадь кухни:', $kitchen_square, ' м²', $br);?>
   <?php itemView('Жилая площадь:', $living_square, ' м²', $br);?>
   <?php itemView('Высота полтолков:', $ceiling_height, 'м', $br);?> 
   <?php itemView('Этаж:', $floor, '', '');?> из <?php echo $floors_quantity; ?></br/>
   <?php itemView('Балкон/лоджия:', $balcony, '', $br);?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Тип комнат:', $rooms_type, '', $br);?>
   <?php itemView('Санузел:', $wc, '', $br);?>
   <?php itemView('Окна:', $window_view, '', $br);?>
   <?php itemView('Ремонт:', $interior, '', $br);?>
   <?php itemView('Способ продажи:', $deal_type, '', $br);?>
   <?php itemView('Мебель:', $furniture, '', $br);?>
   <?php itemView('Техника:', $technics, '', $br);?>
   <?php itemView('Дополнительно:', $additional, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">О доме</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Тип дома: ', $property_type, '', $br); ?> 
   <?php itemView('Пассажирский лифт: ', $passanger_elevator, '', $br); ?> 
   <?php itemView('Грузовой лифт: ', $cargo_elevator, '', $br); ?> 
   <?php itemView('Год постройки:', $year, '', $br);?>
   <?php itemView('Запланирован снос:', $demolition, '', $br);?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('В доме:', $at_building, '', $br); ?> 
   <?php itemView('Двор: ', $outside, '', $br); ?>
   <?php itemView('Парковка: ', $parking, '', $br); ?>
  </div>
  <div class="clear"></div>
 </div>
</div>
<?php //setPostTitle(); ?>
<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[custom-yarpp]');?>
  <div class="clear"></div>
 </div>
</div>