<?php include ('meta-fields.php'); ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <div class="kbrd-breadcrumbs"><?php echo do_shortcode('[flexy_breadcrumb]'); ?></div>
  <div class="ad-bold">
  <?php echo get_the_title(); ?> <?php echo $property_square; ?> м²<br />
  </div>
 </div>
 <div class="kbrd-column col2_5">
  <div class="ad-bold">
   <?php echo $price; ?> ₽ <?php echo $price_param; ?><br/>
   <?php calcSquarePrice($price, $property_square, 'м²'); ?>
  </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[wbcr_php_snippet id="5639"]'); ?>
 </div>
 <div class="kbrd-column col2_5">
  <div class="btn" style="height: 80px; text-align: center;">
   <span class="hide-tail ad-bold" style="color: #fff;"><?php echo $phone; ?></span><br />
   <a class="click-tel" style="color: #fff;" href="#">показать телефон</a>
  </div>
  Контактное лицо<br/>
	 <?php echo $contact_person; ?><br />
  <span>№: </span><?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О помещении</div>
  <div class="kbrd-column col1_2">
   <span>Класс:</span> А
   
   <?php itemView('Площадь участка:', $area_square, 'м²<br />');?>
 </div>
  <div class="kbrd-column col1_2">
  <?php itemView('Этажей в доме:', $floors_quantity, '<br />');?>
  <?php itemView('Материал стен:', $walls, '<br />');?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>
<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->
<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<?php avada_render_related_posts(get_post_type()); // Render Related Posts. 
?>