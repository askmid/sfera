<?php include __DIR__ . '/../meta-fields.php'; ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <div class="kbrd-breadcrumbs"><?php echo do_shortcode('[flexy_breadcrumb]'); ?></div>
  <div class="ad-bold">
   Общепит, <?php echo $property_square; ?> м²<br />
  </div>
 </div>
 <div class="kbrd-column col2_5">
  <span class="pre-price"><?php calcSquarePrice($price, $property_square, 'м²'); ?></span>
  <div class="ad-bold">
   <?php dividePrice($price); ?> <?php haveValue($price_param); ?><br/>
  </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
  Контактное лицо<br/>
	 <?php echo $contact_person; ?><br />
  <span>№: </span><?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О помещении</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Общая площадь:', $property_square, 'м²', $br);?>
   <?php itemView('Этаж:', $floor, '', $br);?>
   <?php itemView('Отделка:', $finishing, '', $br);?>
 </div>
  <div class="kbrd-column col1_2">
  <?php itemView('Планировка:', $layout, '', $br);?>
  <?php itemView('Тип сделки:', $deal_type, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>
<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->
<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О здании</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Готовность:', $readiness, '', $br);?>
   <?php itemView('Тип здания:', $object_type, '', $br);?>
 </div>
  <div class="kbrd-column col1_2">
  <?php itemView('Парковка:', $parking, ', '.$parking_options, $br);?>
  <?php itemView('Количество мест:', $parking_quantity, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>