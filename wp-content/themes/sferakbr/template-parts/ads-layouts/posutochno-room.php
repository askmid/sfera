<?php include __DIR__ . '/../meta-fields.php'; ?>


<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
   Комната <?php echo $property_square; ?>м²,
   в <?php echo $room_quantity; ?>-к.,
   <?php echo $floor; ?>/<?php echo $floors_quantity; ?> эт.
  </div>
 </div>
 <div class="kbrd-column col2_5">
 <span class="pre-price"></span>
  <div class="ad-bold">
   <?php dividePrice($price); ?> за сутки
  </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
  <?php echo $contact_person; ?><br />
  №: <?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О комнате</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Площадь комнаты:', $property_square, ' м²', $br);?> 
   <?php itemView('Комнат в квартире:', $room_quantity, '', $br);?>
   <?php itemView('Количество кроватей:', $beds_quantity, '', $br);?>
   <?php itemView('Комнат спальных мест:', $sleeps_quantity, '', $br);?>
   <?php itemView('Мультимедиа:', $internet, '', $br);?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Этаж:', $floor, '', '');?> из <?php itemView('', $floors_quantity, '', $br);?>
   <?php itemView('Тип дома: ', $property_type, '', $br); ?>
   <?php itemView('Бытовая техника:', $technics, '', $br);?>
   <?php itemView('Комфорт:', $comfort, '', $br);?>
   <?php itemView('Дополнительно:', $kbrd_additional, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>
