<?php include __DIR__ . '/../meta-fields.php'; ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
   <?php echo $room_quantity; ?>-к квартира,
   <?php echo $property_square; ?>м²,
   <?php echo $floor; ?>/<?php echo $floors_quantity; ?> эт.
  </div>
 </div>
 <div class="kbrd-column col2_5">
 <span class="pre-price"><?php getDeposite($deposite); ?><?php calcComission ($price, $comission); ?></span>
  <div class="ad-bold">
  <?php dividePrice($price); ?>/мес
  </div>
  <?php itemView('', $bills, '', '');?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
  <?php echo $contact_person; ?><br />
  <?php echo $ownership_type; ?><br />
  №: <?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О квартире</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Количество комнат:', $room_quantity, '', $br);?>
   <?php itemView('Общая площадь:', $property_square, ' м²', $br);?>
   <?php itemView('Площадь кухни:', $kitchen_square, ' м²', $br);?>
   <?php itemView('Мебель:', $furniture, '', $br);?>
   <?php itemView('Интернет:', $internet, '', $br);?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Этаж:', $floor, '', '');?> из <?php echo $floors_quantity; ?><br /> 
   <?php itemView('Тип комнат:', $rooms_type, '', $br);?>
   <?php itemView('Ремонт:', $kbrd_interior, '', $br);?>
   <?php itemView('Техника:', $technics, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>
<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->
<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">О доме</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Тип дома:', $property_type, '', $br );?>
   <?php itemView('Дополнительно: ', $additional, '', $br); ?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Двор: ', $outside, '', $br); ?>
   <?php itemView('Парковка: ', $parking, '', $br); ?>
  </div>
  <div class="clear"></div>
 </div>
</div>