<?php include __DIR__ . '/../meta-fields.php'; ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
   <?php echo $room_quantity; ?>-к квартира,
   <?php echo $property_square; ?>м²,
   <?php echo $floor; ?>/<?php echo $floors_quantity; ?> эт.
  </div>
 </div>
 <div class="kbrd-column col2_5">
 <span class="pre-price"><?php itemView('Залог:',$deposite, ' ₽', $br );?></span> 
  <div class="ad-bold">
  <?php dividePrice($price); ?>/сут
 </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
	 <?php echo $contact_person; ?><br />
  Арендодатель<br/>
  №: <?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О квартире</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Количество комнат:', $room_quantity, '', $br );?><br /> 
   <?php itemView('Общая площадь:', $property_square, '', $br );?> м²<br />
   <?php itemView('Этаж:', $floor, '', '' );?> из <?php echo $floors_quantity; ?><br />
   <?php itemView('Балкон:', $balcony, '', $br );?>
  </div>
  <div class="kbrd-column col1_2">
  <?php itemView('Техника:', $technics, '', $br );?>
  <?php itemView('Интернет и ТВ:', $internet, '', $br );?>
  <?php itemView('Комфорт:', $comfort, '', $br );?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">Правила</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Количество гостей: ', $guests_quantity, '', $br ); ?>
   <?php itemView('Можно с детьми: ', $kids, '', $br); ?>
   <?php itemView('Можно с животными: ', $pets, '', $br); ?>
  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Можно курить : ', $smoke, '', $br); ?>
   <?php itemView('Разрешены вечеринки: ', $party, '', $br); ?>
   <?php itemView('Есть отчётные документы: ', $docs, '', $br); ?>
  </div>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">О доме</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Парковка: ', $parking, '<span> ('. $free_for_guests.')</span>', $br ); ?>
   <?php itemView('Количество спальных мест: ', $sleeps_quantity, '', $br); ?>

  </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Дополнительно: ', $additional, '', $br); ?><br/>
   <?php itemView('', $facilities, '', $br); ?><br/>
  </div>
  <div class="clear"></div>
 </div>
</div>