<?php include ('meta-fields.php'); ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[flexy_breadcrumb]'); ?>
  <div class="ad-bold">
   <?php echo $district; ?>,
   <?php echo $room_quantity; ?>-к квартира,
   <?php echo $property_square; ?>м²,
   <?php echo $floor; ?>/<?php echo $floors_quantity; ?> эт.
  </div>
 </div>
 <div class="kbrd-column col2_5">
  <div class="ad-bold">
   <?php echo $price; ?> ₽
  </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
  <?php echo do_shortcode('[wbcr_php_snippet id="5639"]'); ?>
 </div>
 <div class="kbrd-column col2_5">
  <div class="btn" style="height: 100px; text-align: center;">
   <span class="hide-tail ad-bold" style="color: #fff;"><?php echo $phone; ?></span><br />
   <a class="click-tel" style="color: #fff;" href="#">показать телефон</a>
  </div>
  <?php echo $contact_person; ?><br />
  <?php echo $ownership_type; ?><br />
  №: <?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О квартире</div>
  <div class="kbrd-column col1_2">
   Тип дома: <?php echo $property_type; ?><br />
   Этаж: <?php echo $floor; ?> из <?php echo $floors_quantity; ?><br />
   Лифт в доме: <?php echo $elevator; ?><br />
   Балкон/лоджия: <?php echo $balcony; ?><br />
   Номер квартиры: <?php echo $flat_num; ?>
  </div>
  <div class="kbrd-column col1_2">
   Количество комнат: <?php echo $room_quantity; ?><br />
   Общая площадь: <?php echo $property_square; ?> м²<br />
   Площадь кухни: <?php echo $kitchen_square; ?> м²<br />
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->


<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col1_1">
  <div class="kbrd-title">Дополнительная информация</div>
  <div class="kbrd-column col1_2">
   <?php //notEmpty($at_building_field, $at_building, 'В доме:'); ?><br/>
   <?php //notEmpty($parking_field, $parking, 'Парковка: '); ?><br/>
   <?php //notEmpty($rooms_type_field, $rooms_type, 'Тип комнат: '); ?><br/>
   <?php //notEmpty($wc_field, $wc, 'Санузел: '); ?><br/>
  </div>
  <div class="kbrd-column col1_2">
   <?php //notEmpty($outside_field, $outside_field, 'Во дворе: '); ?><br/>
   <?php //notEmpty($interior_field, $interior, 'Ремонт: '); ?><br/>
   <?php //notEmpty($window_view_field, $window_view, 'Вид из окна: '); ?><br/>
   <?php //notEmpty($additional_field, $additional, 'Дополнительно: '); ?><br/>
  </div>
  <div class="clear"></div>
 </div>
</div>

<?php avada_render_related_posts(get_post_type()); // Render Related Posts. 
?>