<?php include __DIR__ . '/../meta-fields.php'; ?>

<div class="kbrd-container alignment_bottom">
 <div class="kbrd-column col3_5">
  <div class="kbrd-breadcrumbs"><?php echo do_shortcode('[flexy_breadcrumb]'); ?></div>
  <div class="ad-bold">
  <?php echo $housing_type; ?> <?php echo $property_square; ?>м²
  на участке <?php echo $area_square; ?> сот.
  </div>
 </div>
 <div class="kbrd-column col2_5">
 <span class="pre-price"><?php calcSquarePrice($price, $property_square, 'м²'); ?></span>
 <div class="ad-bold">
   <?php dividePrice($price); ?>
  </div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top">
 <div class="kbrd-column col3_5">
 <?php echo Slider('lGSlider'); ?>
 </div>
 <div class="kbrd-column col2_5">
 <div class="phone-btn">
   <div class="hide-phone ad-bold" style="color: #fff;">+7<?php echo $phone; ?></div><br />
 </div>
	 <?php echo $contact_person; ?><br />
  <?php echo $ownership_type; ?><br />
  <span>№: </span><?php echo $ad_id; ?>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">О доме</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Количество комнат:', $room_quantity,'', $br);?>
   <?php itemView('Площадь дома:', $property_square, ' м²', $br);?>
   <?php itemView('Площадь участка:', $area_square, ' м²', $br);?>
   <?php itemView('Этажей в доме:', $floors_quantity,'', $br);?>
	 </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Категория земель:', $area_type, '', $br);?>
   <?php itemView('Материал стен:', $walls, '', $br);?>
   <?php itemView('Ремонт:', $interior, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Дополнительно</div>
  <div class="kbrd-column col1_2">
   <?php itemView('Терраса или веранда:', $veranda,'', $br);?>
   <?php itemView('Санузел:', $wc, '', $br);?>
   <?php itemView('Парковка:', $parking, '', $br);?>
   <?php itemView('Баня, сауна, бассейн:', $sauna,'', $br);?>
	 </div>
  <div class="kbrd-column col1_2">
   <?php itemView('Коммуникации:', $communication, '', $br);?>
   <?php itemView('Транспортная доступность:', $accessibility, '', $br);?>
   <?php itemView('Инфраструктура:', $infrastructure, '', $br);?>
   <?php itemView('Интернет и ТВ:', $internet, '', $br);?>
  </div>
  <div class="clear"></div>
 </div>
 <div class="clear"></div>
</div>
<!-- YA MAP -->

<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Расположение объекта</div>
  <div class="location">
       <?php itemView('Адрес:', $address, '', $br);?>
   <!--  <?php echo $address; ?> -->
</div>

  <div id="map" style="width: 600px; height: 400px"></div>
  <div class="clear"></div>
 </div>
</div>
<!-- END MAP  -->
<div class="kbrd-container alignment_top space20">
 <div class="kbrd-column col3_5">
  <div class="kbrd-title">Описание</div>
  <?php echo $description; ?>
  <div class="clear"></div>
 </div>
</div>