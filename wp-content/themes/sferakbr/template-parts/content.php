<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sferakbr
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
   <?php 
      $categories = getCategoriesSlugs();
		foreach ($categories as $cat) {
			if(in_category($cat)) {
			include 'ads-layouts/'.$cat.'.php';
			break;
			}

		}
      //include '/var/www/u1395577/data/www/sferakbr.ru/wp-content/themes/sferakbr/all-layouts.php';
	?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
