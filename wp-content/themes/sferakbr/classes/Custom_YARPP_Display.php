<?php

/**
 * @since 3.4 Put everything YARPP into an object, expected to be a singleton global $yarpp.
 */
class Custom_YARPP_Display extends YARPP {

	public $AMPLITUDE = 600001;


	public function display_related( $reference_ID = null, $args = array(), $echo = true ) {

    /** @global $yarpp YARPP */
		global $yarpp;

		// Avoid infinite recursion here.
		if ( $this->do_not_query_for_related() ) {
			return false;
		}
		$this->parse_json_arg($args, 'weight');
		$this->parse_json_arg($args, 'require_tax');
		// Custom templates require .php extension.
		if ( isset( $args['template'] ) && $args['template'] ) {
			// Normalize parameter.
			if ( ( strpos( $args['template'], 'yarpp-template-' ) === 0 ) && ( strpos( $args['template'], '.php' ) === false ) ) {
				$args['template'] .= '.php';
			}
		}
		wp_register_style( 'yarppRelatedCss', plugins_url( '/style/related.css', YARPP_MAIN_FILE ), array(), YARPP_VERSION );
		/**
		 * Filter to allow dequeing of related.css.
		 *
		 * @param boolean default true
		 */
		$enqueue_related_style = apply_filters( 'yarpp_enqueue_related_style', true );
		if ( true === $enqueue_related_style ) {
			wp_enqueue_style( 'yarppRelatedCss' );
		}

		if ( is_numeric( $reference_ID ) ) {
			$reference_ID = (int) $reference_ID;
		} else {
			$reference_ID = get_the_ID();
		}

		/**
		 * @since 3.5.3 don't compute on revisions.
		 */
		if ( $the_post = wp_is_post_revision( $reference_ID ) ) {
			$reference_ID = $the_post;
		}

		$yarpp->setup_active_cache( $args );

		$options = array(
			'limit',
			'order',
			'optin',
		);

		extract( $yarpp->parse_args( $args, $options ) );

		$cache_status = $yarpp->active_cache->enforce( $reference_ID, false, $args );
		if ( $cache_status === YARPP_DONT_RUN ) {
			return;
		}
		if ( $cache_status !== YARPP_NO_RELATED ) {
			$yarpp->active_cache->begin_yarpp_time( $reference_ID, $args );
		}

		$yarpp->save_post_context();

		global $wp_query;
		$wp_query = new WP_Query();

		if ( $cache_status !== YARPP_NO_RELATED ) {
			$orders = explode( ' ', $order );
			
			$template = array(
				'p'          => $reference_ID,
				'meta_query' => array(
					'relation' => 'AND',
					// 'price'    => array(
					// 	'key'     => 'kbrd_price',
					// 	'compare' => 'EXISTS',
					// )
				),
				'orderby'    => array(),
				'showposts'  => $limit,
				'post_type'  => $yarpp->get_query_post_types( $reference_ID, $args ),
			);
		
			$meta_query = $this->meta_query($template, $reference_ID);

			//var_dump($meta_query); die;
			
			$wp_query->query($meta_query);
		}

		$this->prep_query_custom( $yarpp->current_query->is_feed );

		$wp_query->posts = apply_filters(
			'yarpp_results',
			$wp_query->posts,
			array(
				'function'   => 'display_related',
				'args'       => $args,
				'related_ID' => $reference_ID,
			)
		);

		$related_query = $wp_query; // backwards compatibility

		if ( $cache_status !== YARPP_NO_RELATED ) {
			$yarpp->active_cache->end_yarpp_time();
		}
		if ( isset( $args['generate_missing_thumbnails'] ) ) {
			$yarpp->generate_missing_thumbnails = $args['generate_missing_thumbnails'];
		}
		// Be careful to avoid infinite recursion, because those templates might show each related posts' body or
		// excerpt, which would trigger finding its related posts, which would show its related posts body or excerpt...
		$yarpp->rendering_related_content = true;

		$output = $this->get_template_content($reference_ID, $args);

		$yarpp->rendering_related_content = false;

		unset( $related_query );
		$yarpp->restore_post_context();

		if ( $echo ) {
			echo $output;
		}
		return $output;
	}

    public function prep_query_custom( $is_feed = false ) {
		global $wp_query;
		$wp_query->in_the_loop = true;
		$wp_query->is_feed     = $is_feed;

		/*
		 * Make sure we get the right is_single value (see http://wordpress.org/support/topic/288230)
		 */
		$wp_query->is_single = false;
	}


	public function meta_query($template, $post_id) 
	{
		$categories = get_the_category($post_id);
		$cat_id     = $categories[0]->cat_ID;
		$meta       = get_post_meta($post_id);

		//var_dump($meta); die;

		if (in_array($cat_id, array(12,13))) return $this->roomsClause($template, $meta);


	}
	

	private function roomsClause($template, $meta) 
	{
		$template['meta_query']['rooms'] = array(
			'key'   => 'kbrd_rooms_quantity',
			'value' => $meta['kbrd_rooms_quantity'][0],
		);
		$template['meta_query']['price'] = array(
			'key'     => 'kbrd_price',
			'value'   => array($meta['kbrd_price'][0] - $this->AMPLITUDE, $meta['kbrd_price'][0] + $this->AMPLITUDE),
			'compare' => 'BETWEEN',
		);
		$template['orderby'] = array(
			'price' => 'ASC',
		);
		return $template;
	}


}//end of class
