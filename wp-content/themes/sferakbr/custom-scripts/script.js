jQuery(document).ready(function($) {
    jQuery(".lSlider").lightSlider({
		item:1,
		loop:true,
        });
    jQuery(".lGSlider").lightSlider({
		item:1,
  gallery: true,
		loop:true,
		onSliderLoad: function(el) {
   el.lightGallery();
  } 
		});
	 jQuery( ".slide-toggle" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
	  jQuery( ".snav-content" ).slideToggle(); // плавно скрываем, или отображаем все элементы <div>
	 });
	

/* Hide phone number */
  $.fn.textToggle = function(d, b) { 
   return this.each(function(f, a) { 
    a = $(a); 
    var c = $(d).eq(f), 
    g = [b, c.text()];
    c.text(b).show(); 
    $(a).click(function(b) { 
     // b.preventDefault(); 
     c.text(g.reverse()[0]); 
    }) 
   }) 
  }; 
 $(function(){ 
 $('.hide-phone').textToggle(".hide-phone","+7XXXXXXXXXX")  });
	

// /* Автовыбор категории в размещении объявления */
// function autoChoose() {
//  var c = document.querySelector("input[name='category[]']");
//  if (!c.checked) c.checked = true;  // поставить checked, если он не установлен
// }
// autoChoose();


/* Phone field mask */
$(function(){
 var c = document.querySelector("#kbrd_phone_165");
 $(c).mask("+7 (999) 999-99-99");
});

/* Help text on hover */
$(".qmark").hover(function(){
$(this).parents(".has_question").toggleClass("show-help");
//$(".test .wpuf-help").toggleClass("block");  //Add the active class to the area is hovered
});

/* Subcategory on click */
$(".level-1 a").click(function(e){
 e.preventDefault();
 $(".level-2").toggleClass("block");  //Add the active class to the area is hovered
});

/* DEPENDENT HEIGHT */
// jQuery(function(){
//  jQuery('.lSlider ').height($('.lSlider li').width()/3.5);

//  jQuery(window).resize(function(){
//   jQuery('.lSlider').height($('.lSlider li').width()/3.5);
//  });
// });




});

