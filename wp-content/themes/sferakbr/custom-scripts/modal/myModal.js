jQuery(document).ready(function() {
 jQuery('a.myLinkModal').click( function(event){
   event.preventDefault();
   jQuery('#myOverlay').fadeIn(297, function(){
    jQuery('#myModal') 
     .css('display', 'block')
     .animate({opacity: 1}, 198);
   });
 });

 jQuery('#myModal__close, #myOverlay').click( function(){
  jQuery('#myModal').animate({opacity: 0}, 198,
     function(){
      jQuery(this).css('display', 'none');
      jQuery('#myOverlay').fadeOut(297);
   });
 });
});